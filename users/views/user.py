import json

from config import settings
from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import generic
from users.models import User, Location


@method_decorator(csrf_exempt, name='dispatch')
class UserListView(generic.ListView):
    model = User

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)

        self.object_list = self.object_list.prefetch_related('location').order_by('username')

        paginator = Paginator(self.object_list, settings.PAGE_ON_TOTAL)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        users = []
        for user in page_obj:
            users.append({
                'id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'username': user.username,
                'role': user.role,
                'age': user.age,
                'location': list(user.location.all().values_list('name', flat=True)),
            })

        response = {
            "items": users,
            "num_pages": paginator.num_pages,
            "total": paginator.count
        }

        return JsonResponse(response, safe=False)


class UserDetailView(generic.DetailView):
    model = User

    def get(self, request, *args, **kwargs):
        user = self.get_object()

        response = {
            "id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "username": user.username,
            "role": user.role,
            "age": user.age,
            "location": list(user.location.all().values_list('name', flat=True)),
        }
        return JsonResponse(response, safe=True)


@method_decorator(csrf_exempt, name='dispatch')
class UserCreateView(generic.CreateView):
    model = User

    def post(self, request, *args, **kwargs):
        user_data = json.loads(request.body)

        location_obj, _ = Location.objects.get_or_create(name=user_data['locations'])

        user = User.objects.create(
            first_name=user_data['first_name'],
            last_name=user_data['last_name'],
            username=user_data['username'],
            password=user_data['password'],
            role=user_data['role'],
            age=user_data['age'],

        )
        user.location.add(location_obj)

        response = {
            "id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "username": user.username,
            "role": user.role,
            "age": user.age,
            "location": list(user.location.all().values_list('name', flat=True)),
        }

        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class UserUpdateView(generic.UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'username', 'password', 'role', 'age', 'location']

    def patch(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)

        user_data = json.loads(request.body)

        if first_name := user_data.get('first_name'):
            self.object.first_name = first_name
        if last_name := user_data.get('last_name'):
            self.object.last_name = last_name
        if username := user_data.get('username'):
            self.object.username = username
        if password := user_data.get('password'):
            self.object.password = password
        if role := user_data.get('role'):
            self.object.role = role
        if age := user_data.get('age'):
            self.object.age = age
        if location := user_data.get('location'):
            self.object.location, _ = Location.objects.get_or_create(**location)

        self.object.save()

        response = {
            "id": self.object.id,
            "first_name": self.object.first_name,
            "last_name": self.object.last_name,
            "username": self.object.username,
            "role": self.object.role,
            "age": self.object.age,
            "location": list(self.object.location.all().values_list('name', flat=True)),
        }

        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class UserDeleteView(generic.DeleteView):
    model = User
    success_url = '/'

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)

        return JsonResponse({'status': 'ok'}, status=200)
