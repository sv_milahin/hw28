from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from users.models.locations import Location


# Create your models here.

class User(AbstractBaseUser):
    class Role(models.TextChoices):
        ADMIN = 'AD', 'admin',
        MEMBER = 'MB', 'member',
        MODERATOR = 'MO', 'moderator'
    password = models.CharField(max_length=30)
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    age = models.PositiveIntegerField()
    role = models.CharField(max_length=2,
                            choices=Role.choices,
                            default=Role.MEMBER)
    location = models.ManyToManyField(Location, related_name='location_user')

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username
