from django.contrib import admin

from users.models import User, Location


# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id','username', 'first_name', 'last_name', 'age', 'role',]
    list_filter = ['role',]
    search_fields = ['role', 'username']


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    list_filter = ['name',]
    prepopulated_fields = {'slug': ('name',)}


