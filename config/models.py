from django.db import models


class BaseModel(models.Model):
    name = models.CharField(max_length=50, null=False)
    slug = models.SlugField(max_length=200, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True