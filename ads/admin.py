from django.contrib import admin

# Register your models here.
from .models.ads import Ad
from .models.categorys import Category


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'is_published']
    list_filter = ['name', 'price', 'is_published']
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'created', 'updated']
    list_filter = ['name', 'slug', 'created', 'updated']
    prepopulated_fields = {'slug': ('name',)}
    date_hierarchy = 'created'
    search_fields = ['name', 'slug']

