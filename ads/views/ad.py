import json

from ads.models import Ad, Category
from config import settings
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import generic
from users.models import User


@method_decorator(csrf_exempt, name='dispatch')
class AdListView(generic.ListView):
    model = Ad

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)

        self.object_list = self.object_list.order_by('-price')

        paginator = Paginator(self.object_list, per_page=settings.PAGE_ON_TOTAL)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        ads = [{
            'id': item.id,
            'name': item.name,
            'author': {
                'username': item.author.username,
                'last_name': item.author.last_name,
                'first_name': item.author.first_name,
            },
            'price': item.price,
            'description': item.description,
            'is_published': item.is_published,
            'image': item.image.url if item.image else None,
            'created': item.created,
            'updated': item.updated,
            'category': {
                'name': item.category.name
            }
        } for item in page_obj]

        response = {
            'total': paginator.count,
            'num_pages': paginator.num_pages,
            'items': ads,
        }

        return JsonResponse(response, safe=False)


class AdDetailView(generic.DetailView):
    model = Ad

    def get(self, request, *args, **kwargs):
        ad = self.get_object()

        response = {
            'id': ad.id,
            'name': ad.name,
            'author': {
                'username': ad.author.username,
                'last_name': ad.author.last_name,
                'first_name': ad.author.first_name,
            },
            'price': ad.price,
            'description': ad.description,
            'is_published': ad.is_published,
            'image': ad.image.url if ad.image else None,
            'created': ad.created,
            'updated': ad.updated,
            'category': {
                'name': ad.category.name
            }}
        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class AdCreateView(generic.CreateView):
    model = Ad

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        try:
            user_obj = User.objects.get(id=data['author'])
        except User.DoesNotExist:
            return JsonResponse({'error': 'User not found'}, status=404)


        category_obj, _ = Category.objects.get_or_create(name=data['category'])


        ad = Ad.objects.create(
            name=data['name'],
            author=user_obj,
            price=data['price'],
            description=data['description'],
            is_published=data['is_published'],
            image=data['image'],
            category=category_obj
        )

        response = {
            'id': ad.id,
            'name': ad.name,
            'author': {
                'username': ad.author.username,
                'last_name': ad.author.last_name,
                'first_name': ad.author.first_name,
            },
            'price': ad.price,
            'description': ad.description,
            'is_published': ad.is_published,
            'image': ad.image.url if ad.image else None,
            'created': ad.created,
            'updated': ad.updated,
            'category': {
                'name': ad.category.name
            }}
        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class AdUpdateView(generic.UpdateView):
    model = Ad
    fields = ['name', 'author', 'price', 'description', 'is_published', 'image', 'category']

    def patch(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)

        data = json.loads(request.body)
        self.object.name = data.get("name")
        self.object.price = data.get("price")
        self.object.description = data.get("description")
        self.object.is_published = data.get("is_published")

        self.object.save()

        response = {
            'id': self.object.id,
            'name': self.object.name,
            'author': {
                'username': self.object.author.username,
                'last_name': self.object.author.last_name,
                'first_name': self.object.author.first_name,
            },
            'price': self.object.price,
            'description': self.object.description,
            'is_published': self.object.is_published,
            'image': self.object.image.url if self.object.image else None,
            'created': self.object.created,
            'updated': self.object.updated,
            'category': {
                'name': self.object.category.name
            }}
        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class AdDeleteView(generic.DeleteView):
    model = Ad
    success_url = '/'

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)

        return JsonResponse({'status': 'ok'}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class AdImageView(generic.UpdateView):
    model = Ad
    fields = ['name', 'author', 'price', 'description', 'is_published', 'image', 'category']

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        self.object.image = request.FILES['image']
        self.object.save()

        response = self.object.serialize()
        response['image'] = self.object.image.url if self.object.image else None

        return JsonResponse(response, safe=False)