import json

from ads.models import Category
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import generic


class CategoryListView(generic.ListView):
    model = Category

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        self.object_list = self.object_list

        respons = [{
            'id': item.id,
            'name': item.name,
            'slug': item.slug,
            'created': item.created,
            'updated': item.updated,
        } for item in self.object_list]

        return JsonResponse(respons, safe=False)


class CategoryDetailView(generic.DetailView):
    model = Category

    def get(self, request, *args, **kwargs):
        category = self.get_object()

        response = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'created': category.created,
            'updated': category.updated,
        }

        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class CategoryCreateView(generic.CreateView):
    model = Category

    def post(self, request, *args, **kwargs):
        category_data = json.loads(request.body)

        category = Category.objects.create(
            name=category_data['name'],
            slug=category_data['slug'],
        )

        response = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'created': category.created,
            'updated': category.updated,
        }

        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class CategoryUpdateView(generic.UpdateView):
    model = Category
    fields = ['name', 'slug']

    def patch(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)

        data = json.loads(request.body)

        self.object.name = data['name']
        self.object.name = data['slug']

        self.object.save()

        response = {
            'id': self.object.id,
            'name': self.object.name,
            'slug': self.object.slug,
            'created': self.object.created,
            'updated': self.object.updated,
        }

        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class CategoryDeleteView(generic.DeleteView):
    model = Category
    success_url = "/"

    def delete(self, request, *args, **kwargs):
        super().delete(request, *args, **kwargs)

        return JsonResponse({'status': 'ok'}, status=200)
